<?php
echo "<h1>Versão Atual do PHP: " . phpversion() . "</h1>";

$servername = "mysql";
$username = "root";
$password = "senha";
$dbname = "oficina";

try {
  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
  // set the PDO error mode to exception
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $version = $conn->query("select version()")->fetchColumn();
  echo "<h1>Conexão com banco OK - Versão " . $version ."</h1>";
} catch(PDOException $e) {
  echo "Connection failed: " . $e->getMessage();
}
?>

